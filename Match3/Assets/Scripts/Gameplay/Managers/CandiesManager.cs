using System.Collections.Generic;
using UnityEngine;
using Match3.Gameplay.Entities;

namespace Match3.Gameplay.Managers
{
    public class CandiesManager : MonoBehaviourSingleton<CandiesManager>
    {
        private List<Candy> allCandies = new List<Candy>();
        private List<Candy> selectedCandies = new List<Candy>();
        private Candy actualTypeSelection = null;
        private float lastLine = 0;

        [SerializeField] private int minimumCombination = 3;
        [SerializeField] private int scorePerCandy = 10;

        private void Update()
        {
            if (selectedCandies.Count == 1)
            {
                actualTypeSelection = selectedCandies[0];
            }

            SetSounds();
        }

        private void SetSounds()
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (selectedCandies.Count > 0)
                {
                    if (selectedCandies.Count >= minimumCombination)
                    {
                        if (AudioManager.Get() != null)
                        {
                            AudioManager.Get().Play("Correct");
                        }
                    }
                    else
                    {
                        if (AudioManager.Get() != null)
                        {
                            AudioManager.Get().Play("Wrong");
                        }
                    }
                }
            }
        }

        public void SetLastLine(float line)
        {
            lastLine = line;
        }

        public float GetLastLine()
        {
            return lastLine;
        }

        // allCandies List
        public void AddAllCandiesToTheList(Candy candy)
        {
            allCandies.Add(candy);
        }

        public float GetQuantityOfAllCandies()
        {
            return allCandies.Count;
        }

        public Candy GetDownCandy(int index)
        {
            return allCandies[index];
        }


        // selectedCandies List
        public void AddCandiesToTheList(Candy candy)
        {
            selectedCandies.Add(candy);
        }

        public void RemoveCandiesToTheList(Candy candy)
        {
            for (int i = 0; i < selectedCandies.Count; i++)
            {
                if (selectedCandies[i] == candy)
                {
                    // Elimino el que quiero remover y todos los que tiene por delante
                    for (int j = selectedCandies.Count - 1; j >= i; j--)
                    {
                        selectedCandies.RemoveAt(j);
                    }

                    i = selectedCandies.Count;
                }
            }
        }

        public bool CheckIfTheCandyIsOnTheList(Candy candy)
        {
            return selectedCandies.Contains(candy);
        }

        public float GetQuantityOfSelectedCandies()
        {
            return selectedCandies.Count;
        }

        public Candy GetCurrentKindOfCandy()
        {
            // Devuelvo el tipo de caramelo que estoy seleccionando actualmente
            return actualTypeSelection;
        }

        public Candy GetPositionOfLastCandy()
        {
            return selectedCandies[selectedCandies.Count - 1];
        }

        public void CheckMatch()
        {
            if (selectedCandies.Count >= minimumCombination)
            {
                if (AudioManager.Get() != null)
                {
                    AudioManager.Get().Play("Correct");
                }

                for (int i = 0; i < selectedCandies.Count; i++)
                {
                    selectedCandies[i].remove = true;
                }
                if (GameManager.Get() != null)
                {
                    GameManager.Get().TakeOneMove();
                    GameManager.Get().AddScore(scorePerCandy * selectedCandies.Count);
                }
            }

            selectedCandies.Clear();
        }
    }
}