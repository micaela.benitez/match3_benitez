using UnityEngine;

namespace Match3.Gameplay.Managers
{
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        [Header("Board data")]
        [SerializeField] [Range(1, 10)] private int width = 10;
        [SerializeField] [Range(1, 10)] private int height = 10;

        [Header("Game Data")]
        [SerializeField] private int totalMoves = 10;
        private int moves = 0;
        private int score = 0;        

        private bool replay = false;

        public void Init()
        {
            moves = totalMoves;
            score = 0;
            replay = false;
        }

        public int GetWidth()
        {
            return width;
        }

        public int GetHeight()
        {
            return height;
        }

        public void TakeOneMove()
        {
            moves--;            
        }

        public int GetMoves()
        {
            return moves;
        }

        public void AddScore(int points)
        {
            score += points;
        }

        public int GetScore()
        {
            return score;
        }

        public void SetReplay(bool state)
        {
            replay = state;
        }

        public bool GetReplay()
        {
            return replay;
        }
    }
}