using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using Match3.Gameplay.Managers;

namespace Match3.Gameplay.Entities
{
    public class Candy : MonoBehaviour
    {
        private enum CANDY { BLUE, BROWN, ORANGE, PINK, RED, VIOLET }
        private CANDY candy = CANDY.BLUE;

        [Header("Candies data")]
        [SerializeField] private List<CANDY> candiesType = new List<CANDY>();
        [SerializeField] private List<Sprite> candyTexture = new List<Sprite>();
        [SerializeField] private List<RuntimeAnimatorController> candyAnimation = new List<RuntimeAnimatorController>();

        private int totalCandies = 6;
        private SpriteRenderer Renderer = null;
        private bool selected = false;
        private float timer = 0;

        [NonSerialized] public bool remove = false;

        private void Awake()
        {
            Renderer = GetComponent<SpriteRenderer>();
        }

        private void Start()
        {
            if (CandiesManager.Get() != null)
            {
                CandiesManager.Get().AddAllCandiesToTheList(this);
            }
            SetRandomCandy();
        }

        private void Update()
        {
            timer += Time.deltaTime;

            // Chequeo si deje de apretar el mouse
            if (Input.GetMouseButtonUp(0))
            {
                MouseUp();
            }
            
            if (CandiesManager.Get() != null)
            {
                if (timer > 0.5)
                {
                    // Chequeo si tengo que mover el caramelo mientras no sea de la ultima linea
                    if (transform.position.y != CandiesManager.Get().GetLastLine())
                    {
                        if (!CheckDownCandy())
                        {
                            MoveCandy();
                        }
                    }

                    // Chequeo que el caramelo este en la lista
                    if (!CandiesManager.Get().CheckIfTheCandyIsOnTheList(this))
                    {
                        Renderer.color = Color.white;
                        selected = false;
                    }
                }
            }

            if (GameManager.Get().GetReplay())
            {
                SetRandomCandy();
            }
        }

        private void SetRandomCandy()
        {
            bool correctType = false;

            do
            {
                candy = (CANDY)Random.Range(0, totalCandies);
                for (int i = 0; i < candiesType.Count; i++)
                {
                    if (candy == candiesType[i])
                    {
                        correctType = true;
                    }
                }

            } while (!correctType);

            GetComponent<SpriteRenderer>().sprite = candyTexture[(int)candy];
            GetComponent<Animator>().runtimeAnimatorController = candyAnimation[(int)candy];
        }

        private void OnMouseDown()
        {
            if (GameManager.Get().GetMoves() > 0)
            {
                Select();
            }
        }

        private void OnMouseEnter()
        {
            if (GameManager.Get().GetMoves() > 0)
            {
                if (Input.GetMouseButton(0))
                {
                    if (CheckIfItIsSelectable())
                    {
                        if (selected)
                        {
                            Deselect();
                        }
                        else
                        {
                            Select();
                        }
                    }
                }
            }
        }

        private void MouseUp()
        {
            if (CandiesManager.Get() != null)
            {
                CandiesManager.Get().CheckMatch();
            }
            Deselect();

            if (remove)   // Muevo las piezas eliminadas para arriba
            {
                transform.position = new Vector2(transform.position.x, transform.position.y + GameManager.Get().GetHeight());
                SetRandomCandy();
                remove = false;
            }
        }

        private void Select()
        {            
            Renderer.color = Color.grey;
            selected = true;
            if (CandiesManager.Get() != null)
            {
                CandiesManager.Get().AddCandiesToTheList(this);
            }

            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Select");
            }
        }

        private void Deselect() 
        {
            Renderer.color = Color.white;
            selected = false;
            if (CandiesManager.Get() != null)
            {
                CandiesManager.Get().RemoveCandiesToTheList(this);
            }

            if (AudioManager.Get() != null)
            {
                if (CandiesManager.Get() != null)
                {
                    if (CandiesManager.Get().GetQuantityOfSelectedCandies() > 0)
                    {
                        AudioManager.Get().Play("Deselect");
                    }
                }
            }
        }

        private bool CheckIfItIsSelectable()
        {
            // Chequeo si ya hay mas 1 caramelo seleccionado
            if (CandiesManager.Get() != null)
            {
                if (CandiesManager.Get().GetQuantityOfSelectedCandies() > 0)
                {
                    Vector2 lastCandyPos = CandiesManager.Get().GetPositionOfLastCandy().transform.position;
                    bool selectable = false;

                    bool up = CheckCandyAround(lastCandyPos.x, lastCandyPos.y + 1);
                    bool down = CheckCandyAround(lastCandyPos.x, lastCandyPos.y - 1);
                    bool right = CheckCandyAround(lastCandyPos.x + 1, lastCandyPos.y);
                    bool left = CheckCandyAround(lastCandyPos.x - 1, lastCandyPos.y);
                    bool diagonalUp = CheckCandyAround(lastCandyPos.x - 1, lastCandyPos.y + 1);
                    bool diagonalDown = CheckCandyAround(lastCandyPos.x + 1, lastCandyPos.y - 1);
                    bool diagonalRight = CheckCandyAround(lastCandyPos.x + 1, lastCandyPos.y + 1);
                    bool diagonalLeft = CheckCandyAround(lastCandyPos.x - 1, lastCandyPos.y - 1);

                    if (up || down || right || left || diagonalUp || diagonalDown || diagonalRight || diagonalLeft)
                    {
                        selectable = true;
                    }

                    // Chequeo si es del tipo actual de caramelos que estoy seleccionando y si es seleccionable
                    if (candy == CandiesManager.Get().GetCurrentKindOfCandy().candy && selectable)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        private bool CheckCandyAround(float posX, float posY)
        {
            if (posX == transform.position.x && posY == transform.position.y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckDownCandy()
        {
            Vector2 posToCheck = new Vector2(transform.position.x, transform.position.y - 1);
            bool existDownCandy = false;

            if (CandiesManager.Get() != null)
            {
                for (int i = 0; i < CandiesManager.Get().GetQuantityOfAllCandies(); i++)
                {
                    if ((Vector2)CandiesManager.Get().GetDownCandy(i).transform.position == posToCheck)
                    {
                        existDownCandy = true;
                    }
                }
            }

            return existDownCandy;
        }

        private void MoveCandy()
        {
            transform.position = new Vector2(transform.position.x, transform.position.y - 1);
        }
    }
}