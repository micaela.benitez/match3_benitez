using UnityEngine;
using Match3.Gameplay.Managers;

namespace Match3.Gameplay.Entities
{
    public class Board : MonoBehaviour
    {
        [Header("Board data")]
        [SerializeField] private GameObject backgroundPrefab = null;
        [SerializeField] private Color cubesColor = Color.white;

        [Header("Candies data")]
        [SerializeField] private Transform candiesContainer = null;
        [SerializeField] private GameObject candyPrefab = null;

        private Vector2 initialPos = new Vector2();
        private float offSet = 0.5f;

        private void Awake()
        {
            GameManager.Get().Init();
        }

        private void Start()
        {
            if (GameManager.Get() != null)
            {
                CalculateInitialPosition(GameManager.Get().GetWidth(), ref initialPos.x);
                initialPos.x = -initialPos.x;
                CalculateInitialPosition(GameManager.Get().GetHeight(), ref initialPos.y);

                for (int i = 0; i < GameManager.Get().GetWidth(); i++)
                {
                    for (int j = 0; j < GameManager.Get().GetHeight(); j++)
                    {
                        Vector2 pos = new Vector2(initialPos.x + i, initialPos.y - j);

                        CreatedBackground(i, j, pos);
                        CreatedCandies(i, j, pos);

                        // Seteo ultima linea
                        if (j == GameManager.Get().GetHeight() - 1)
                        {
                            if (CandiesManager.Get() != null)
                            {
                                if (CandiesManager.Get() != null)
                                {
                                    CandiesManager.Get().SetLastLine(pos.y);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void CreatedBackground(int i, int j, Vector2 pos)
        {
            GameObject background = Instantiate(backgroundPrefab, pos, Quaternion.identity);
            background.transform.parent = gameObject.transform;
            background.name = "Background " + i + ":" + j;
            if ((i % 2 == 0 && j % 2 == 0) || (i % 2 != 0 && j % 2 != 0))
            {
                background.GetComponent<SpriteRenderer>().material.color = cubesColor;
            }
        }

        private void CreatedCandies(int i, int j, Vector2 pos)
        {
            GameObject candy = Instantiate(candyPrefab, pos, Quaternion.identity, candiesContainer);
            candy.name = "Candy " + i + ":" + j;
        }

        private void CalculateInitialPosition(int size, ref float initialPos)
        {
            if (size % 2 == 0)
            {
                initialPos = (size / 2) - offSet;
            }
            else
            {
                initialPos = size / 2;
            }
        }
    }
}