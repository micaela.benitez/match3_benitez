using UnityEngine;
using Match3.Gameplay.Managers;

namespace Match3.UI
{
    public class UIMainMenu : MonoBehaviour
    {
        private void Update()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Menu");
            }
        }

        public void LoadGameScene()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Button");
            }

            if (LoaderManager.Get() != null)
            {
                LoaderManager.Get().LoadScene("Game");
            }
        }

        public void LoadInstructionsScene()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Button");
            }

            if (LoaderManager.Get() != null)
            {
                LoaderManager.Get().LoadScene("Instructions");
            }
        }

        public void LoadCreditsScene()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Button");
            }

            if (LoaderManager.Get() != null)
            {
                LoaderManager.Get().LoadScene("Credits");
            }
        }

        public void ExitGame()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Button");
            }

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}