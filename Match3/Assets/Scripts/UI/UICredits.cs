using UnityEngine;
using Match3.Gameplay.Managers;

namespace Match3.UI
{
    public class UICredits : MonoBehaviour
    {
        private void Start()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Menu");
            }
        }

        public void LoadMainMenuScene()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Button");
            }

            if (LoaderManager.Get() != null)
            {
                LoaderManager.Get().LoadScene("MainMenu");
            }
        }

        public void ExitGame()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Button");
            }

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}