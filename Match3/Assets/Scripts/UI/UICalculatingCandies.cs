using UnityEngine;
using Match3.Gameplay.Managers;

namespace Match3.UI
{

    public class UICalculatingCandies : MonoBehaviour
    {
        private float timer = 0;

        private void Update()
        {
            timer += Time.deltaTime;

            if (timer > 0.5f)
            {
                GameManager.Get().Init();
                gameObject.SetActive(false);
            }
        }
    }
}