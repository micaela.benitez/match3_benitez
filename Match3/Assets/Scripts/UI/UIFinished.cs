using UnityEngine;
using Match3.Gameplay.Managers;

namespace Match3.UI
{

    public class UIFinished : MonoBehaviour
    {
        [SerializeField] private Canvas calculatingCandies;

        public void Replay()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Button");
            }

            if (GameManager.Get() != null)
            {
                GameManager.Get().SetReplay(true);
            }
            
            calculatingCandies.gameObject.SetActive(true);
        }

        public void NoReplay()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Button");
            }

            if (AudioManager.Get() != null)
            {
                AudioManager.Get().StopAllSFX();
            }

            if (LoaderManager.Get() != null)
            {
                LoaderManager.Get().LoadScene("MainMenu");
            }
        }

        public void ExitGame()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Button");
            }

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}