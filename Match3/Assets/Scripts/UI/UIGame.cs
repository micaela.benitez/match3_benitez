using UnityEngine;
using Match3.Gameplay.Managers;
using TMPro;

namespace Match3.UI
{

    public class UIGame : MonoBehaviour
    {
        [SerializeField] private TMP_Text moves = null;
        [SerializeField] private TMP_Text score = null;

        [Header("Finished Canvas Data")]
        [SerializeField] private Canvas finished = null;

        private void Start()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().StopAllSFX();
                AudioManager.Get().Play("Game");
            }
        }

        private void Update()
        {
            if (GameManager.Get() != null)
            {
                moves.text = "" + GameManager.Get().GetMoves();
                score.text = "" + GameManager.Get().GetScore();

                if (GameManager.Get().GetMoves() <= 0)
                {
                    finished.gameObject.SetActive(true);
                }
            }

            if (GameManager.Get().GetReplay())
            {
                finished.gameObject.SetActive(false);
            }
        }

        public void LoadMainMenuScene()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().StopAllSFX();
                AudioManager.Get().Play("Button");
            }

            if (LoaderManager.Get() != null)
            {
                LoaderManager.Get().LoadScene("MainMenu");
            }
        }

        public void ExitGame()
        {
            if (AudioManager.Get() != null)
            {
                AudioManager.Get().Play("Button");
            }

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}